# Sortowanie przez wstawianie O(n2)
# Wejście: lista t, która zawiera n elementów
# Wyjście: posortowana lista t
# 1. j ← n - 1
# 2. Dopóki j ≥ 1
# 2.1. p ← t[j]
# 2.2. i ← j + 1
# 2.3. Dopóki i ≤ n oraz p > t[i]
# 2.3.1. t[i - 1] ← t[i]
# 2.3.2. i ← i + 1
# 2.4. t[i - 1] ← p
# 2.5. j ← j - 1


# Zaimplementuj algorytm sortowania przez wstawianie (dla listy
# indeksowanej od 0) w taki sposób, aby sortował dane rosnąco

t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]
n = len(t)
j = n - 1

while j >= 0:
    p = t[j]
    i = j + 1
    while i < n and p > t[i]:
        t[i-1] = t[i]
        i += 1
    t[i - 1] = p
    j -= 1

print(t)
