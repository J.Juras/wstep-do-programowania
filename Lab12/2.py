# Zaimplementuj algorytm sortowania bąbelkowego (dla listy
# indeksowanej od 0) w taki sposób, aby sortował dane malejąco.

t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]

n = len(t)
j = n - 1

while j >= 1:
    i = 0
    while i < j:
        if t[i] < t[i+1]:
            t[i], t[i+1] = t[i+1], t[i]
        i = i + 1
    j = j - 1

print(t)
