# Zaimplementuj iteracyjną wersję algorytmu przeszukiwania
# binarnego (dla listy indeksowanej od 0) w taki sposób, aby zwrócił indeks
# elementu, na którym znajduje się element. Jeżeli element się nie znajduje
# powinna zostać zwrócone -1 lub wartość None

t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]

def sort_list(list):
    n = len(list)
    j = n - 1
    while j >= 1:
        i = 0
        while i < j:
            if list[i] > list[i + 1]:
                list[i], list[i + 1] = list[i + 1], list[i]
            i = i + 1
        j = j - 1
    return list
def bin_search(list):
    n = len(list)
    k = int(input("Podaj szukany element: "))
    lewo = 0
    prawo = n
    while lewo < prawo:
        srodek = (lewo + prawo) // 2
        if list[srodek] < k:
            lewo = srodek + 1
        else:
            prawo = srodek
    if list[lewo] == k:
        return lewo
    else:
        return None

print(sort_list(t))
print(bin_search(t))
