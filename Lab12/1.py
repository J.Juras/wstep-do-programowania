# Sortowanie bąbelkowe O(n2)
# Wejście: lista t, która zawiera n elementów
# Wyjście: posortowana lista t
# 1. j ← n - 1
# 2. Dopóki j ≥ 1
# 2.1. i ← 1
# 2.2. Dopóki i ≤ j
# 2.2.1. Jeżeli t[i] > t[i + 1]
# 2.2.1.1. zamień(t[i], t[i + 1])
# 2.2.2. i ← i + 1
# 2.3. j ← j - 1

# Zaimplementuj algorytm sortowania bąbelkowego (dla listy
# indeksowanej od 0) w taki sposób, aby sortował dane rosnąco.


t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]

n = len(t)
j = n - 1

while j >= 1:
    i = 0
    while i < j:
        if t[i] > t[i+1]:
            t[i], t[i+1] = t[i+1], t[i]
        i = i + 1
    j = j - 1

print(t)
