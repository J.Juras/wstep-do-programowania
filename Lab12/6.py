# Zaimplementuj algorytm sortowania przez wybór (dla listy
# indeksowanej od 0) w taki sposób, aby sortował dane malejąco.

t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]
n = len(t)
j = 0
while j < n:
    p = j
    i = j + 1
    while i < n:
        if t[i] > t[p]:
            p = i
        i += 1
    t[j], t[p] = t[p], t[j]
    j += 1

print(t)
