# Zaimplementuj algorytm sortowania przez wstawianie (dla listy
# indeksowanej od 0) w taki sposób, aby sortował dane malejąco.

t = [2, 4, 1, 0, 23, 34, 7, 9, 11, 4]
n = len(t)
j = n - 1

while j >= 0:
    p = t[j]
    i = j + 1
    while i < n and p < t[i]:
        t[i-1] = t[i]
        i += 1
    t[i - 1] = p
    j -= 1

print(t)
