def hanoi(n, src, dest, aux):
    if n == 1:
        print("Przenieś dysk 1 ze słupka", src, "na słupek", dest)
        return
    hanoi(n-1, src, aux, dest)
    print("Przenieś dysk", n, "ze słupka", src, "na słupek", dest)
    hanoi(n-1, aux, dest, src)


# Wybór ilości dysków

def ile_dyskow():
    n = int(input("Podaj liczbę dysków: "))
    for i in range(1, n+1):
        dysk = i * "-"
        a.append(dysk)
        b.append("|")
        c.append("|")

    for i in range(n+1):
        if i == 0:
            print(a[i], (n - 1) * " ", b[i], n * " ", c[i])
        else:
            print(a[i], (n - i) * " ", b[i], n * " ", c[i])


def pytanie():
    print("NOWA GRA")
    print("Graj sam (1)     Auto-rozwiązanie (0)")
    b = int(input())
    if b == 1:
        return ile_dyskow()
    elif b == 0:
        return ile_dyskow()


# Instrukcja
def instrukcja():
    print("WIEŻE HANOI")
    print("Celem gry jest przeniesienie całej wieży dysków z słupka A na słupek C,")
    print("poprzez słupek B. UWAGA: ")
    print("1. Możesz przenosić tylko jeden dysk naraz.")
    print("2. Nie możesz położyć większego dysku na mniejszym.")
    print("Powodzenia!")

#
# a = []
# b = []
# c = []
# cel = []
#
#
# n = int(input("Podaj liczbę dysków: "))
# for i in range(1, n + 1):
#     dysk = i
#     a.append(dysk)
#     cel.append(dysk)
#     b.append("|")
#     c.append("|")
#


def gra(a, b, c):
    print(" ")
    s = input("Z którego słupka zdjąć dysk?: ")  # s - source
    d = input("Na który słupek nałożyć dysk?: ")  # d - destination
    s = locals()[s]
    d = locals()[d]
    for i in range(n):
        if s[i] != '|':
            for j in range(n-1, -1, -1):
                if d[j] == '|':
                    print(d[j])
                    s[i], d[j] = d[j], s[i]
                    break
            break
    print(" ")
    print('A', " ", 'B', " ", 'C')
    for i in range(n):
        print(a[i], " ", b[i], " ", c[i])


# while c != cel:
#     gra(a, b, c)
#
# print(" ")
# print("Brawo!!!")

# hanoi(3, "A", "C", "B")


def hanoi2(n, P1, P2, P3):
    """ Move n discs from pole P1 to pole P3. """
    if n == 0:
        # No more discs to move in this step
        return

    global count
    count += 1

    # move n-1 discs from P1 to P2
    hanoi2(n-1, P1, P3, P2)

    if P1:
        # move disc from P1 to P3
        P3.append(P1.pop())
        print(A, B, C)

    # move n-1 discs from P2 to P3
    hanoi2(n-1, P2, P1, P3)

# Initialize the poles: all n discs are on pole A.
n = 3
A = list(range(n,0,-1))
B, C = [], []

print(A, B, C)
count = 0
hanoi2(n, A, B, C)
print(count)



# śmietnik z main.py
def rozwiazanie(n, src, dest, aux):
    if n == 1:
        print("Przenieś dysk 1 ze słupka", src, "na słupek", dest)
        return
    rozwiazanie(n - 1, src, aux, dest)
    print("Przenieś dysk", n, "ze słupka", src, "na słupek", dest)
    rozwiazanie(n - 1, aux, dest, src)


def ile_dyskow2():
    print(" ")
    a = []
    b = []
    c = []
    cel = []
    n = input("Podaj liczbę dysków: ")
    try:                                    # error-handling jeśli gracz wpisze niepoprawną wartość
        n = int(n)
    except ValueError:
        print("Podaj poprawną liczbę.")
        return ile_dyskow()
    if n <= 0:
        print("Podaj poprawną liczbę.")
        return ile_dyskow()
    for i in range(1, n+1):
        a.append(i)
        cel.append(i)
        b.append("|")
        c.append("|")
    return n, a, b, c, cel


def graf2(n, a, b, c, licznik):
    print(" ")
    print('A', " ", 'B', " ", 'C', " ", "Liczba ruchów: ", licznik)
    for i in range(n):
        print(a[i], " ", b[i], " ", c[i])
    print(" ")

