import time     # aby użyć time.sleep()
akcept = ["A", "B", "C", "a", "b", "c", "1", "0"]       # lista przyjmowanych wartości słupków


# Menu
def menu():
    print("WIEŻE HANOI")
    print("Graj (1)     Instrukcja (2)     Wyjdź (0)")
    odp = input()
    while odp not in ["0", "1", "2"]:               # error-handling jeśli gracz wciśnie niepożądany klawisz
        print("\nWciśnij odpowiedni klawisz.")
        odp = input()
    if odp == "1":
        pytanie()
    elif odp == "2":
        zasady()
    elif odp == "0":
        print("\nDo widzenia...")
        time.sleep(1)     # tylko dla kosmetyki, żeby program nie zamykał się od razu
        quit()


# Wybór rozgrywki
def pytanie():
    print("\nNOWA GRA")
    print("Graj sam (1)     Auto-rozwiązanie (0)")
    odp = input()
    while odp not in ["0", "1"]:                    # error-handling jeśli gracz wciśnie niepożądany klawisz
        print("\nWciśnij odpowiedni klawisz.")
        odp = input()
    if int(odp) == 1:
        n, a, b, c, cel = ile_dyskow()
        gra(n, a, b, c, cel)
    elif int(odp) == 0:
        n, a, b, c, cel = ile_dyskow()
        global licznik
        licznik = 0
        m = n
        graf(n, a, b, c, licznik)
        hanoi(n, a, b, c, a, b, c, m)
        print("\nKONIEC")
        input("Wróć do menu ('enter')\n")
        menu()


# Instrukcja
def zasady():
    print("\nWIEŻE HANOI")
    print("Celem gry jest przeniesienie całej wieży dysków z słupka A na słupek C, poprzez słupek B.")
    print("UWAGA: ")
    print("1. Możesz przenosić tylko jeden dysk naraz.")
    print("2. Możesz zdejmować tylko dyski z wierzchu.")
    print("3. Nie możesz położyć większego dysku na mniejszym.")
    print("Wpisuj odpowiednie litery, aby przenosić dyski między słupkami.")
    print("W każdym ruchu możesz wpisać '1', aby wyjść do menu, lub '0', aby opuścić grę.")
    print("Powodzenia!")
    input("\nWróć do menu ('enter')\n")
    menu()


# Wybór ilości dysków
def ile_dyskow():
    a, b, c, cel = [], [], [], []
    n = input("\nPodaj liczbę dysków: ")
    try:                                    # error-handling jeśli gracz wpisze niepoprawną wartość
        n = int(n)
    except ValueError:
        print("\nPodaj poprawną liczbę.")
        return ile_dyskow()
    if n <= 0:
        print("\nPodaj poprawną liczbę.")
        return ile_dyskow()
    for i in range(1, n+1):
        dysk = (i * "-") + (n+2-i) * " "
        slupek = "|" + (n+1) * " "
        a.append(dysk)
        cel.append(dysk)
        b.append(slupek)
        c.append(slupek)
    return n, a, b, c, cel


# Printowanie grafu
def graf(n, a, b, c, licznik):
    print("\n"'A', n * " ", 'B', n * " ", 'C', "   ", "Liczba ruchów: ", licznik)
    for i in range(n):
        print(a[i], b[i], c[i])


# Wywołanie gry
def gra(n, a, b, c, cel):
    licznik = 0                                     # licznik
    min_ruchy = 2 ** n - 1                          # min_ruchy - minimalna ilość ruchów
    print(" ")
    print("Minimalna liczba ruchów: ", min_ruchy)
    while c != cel:
        graf(n, a, b, c, licznik)
        rozgrywka(n, a, b, c)
        licznik += 1
    graf(n, a, b, c, licznik)
    print("\nBRAWO!!!\nWYNIK")
    print("Minimalna liczba ruchów: ", min_ruchy)
    print("Twoja liczba ruchów: ", licznik)
    input("\nWróć do menu ('enter')\n")
    menu()


# Rozgrywka
def rozgrywka(n, a, b, c):
    s = input("\nZ którego słupka zdjąć dysk?: ")     # s - source
    while s not in akcept:
        print("\nPodaj właściwą literę.")
        s = input("Z którego słupka zdjąć dysk?: ")
    if s == "0":
        odp = input("\nWyjść z gry? (t/n) ")
        if odp == "t":
            print("\nDo widzenia...")
            time.sleep(1)
            quit()
        else:
            return rozgrywka(n, a, b, c)
    elif s == "1":
        odp = input("\nWyjść do menu? (t/n) ")
        if odp == "t":
            print(" ")
            return menu()
        else:
            return rozgrywka(n, a, b, c)
    d = input("Na który słupek nałożyć dysk?: ")    # d - destination
    while d not in akcept:
        print("\nPodaj właściwą literę.")
        d = input("Na który słupek nałożyć dysk?: ")
    if d == "0":
        odp = input("\nWyjść z gry? (t/n) ")
        if odp == "t":
            print("\nDo widzenia...")
            time.sleep(1)
            quit()
        else:
            return rozgrywka(n, a, b, c)
    elif d == "1":
        odp = input("\nWyjść do menu? (t/n) ")
        if odp == "t":
            print(" ")
            return menu()
        else:
            return rozgrywka(n, a, b, c)

    s, d = s.lower(), d.lower()         # konwersja dużych liter na małe, aby program rozpoznawał listy
    if s == d:
        print("\nTo ten sam słupek.")
        return rozgrywka(n, a, b, c)
    s = locals()[s]
    d = locals()[d]
    if s[n-1] == "|" + (n+1) * " ":
        print("\nNie ma tu dysków.")
        return rozgrywka(n, a, b, c)

    for i in range(n):
        if s[i] != "|" + (n+1) * " ":
            for j in range(n-1, -1, -1):
                if d[j] == "|" + (n+1) * " ":
                    if j < (n - 1) and d[j + 1] < s[i]:    # jeśli gracz spróbuje położyć większy dysk na mniejszym
                        print("\nNie możesz położyć większego dysku na mniejszym!")
                        return rozgrywka(n, a, b, c)
                    else:
                        s[i], d[j] = d[j], s[i]
                        break
            break


# Auto-rozwiązanie
def hanoi(n, p1, p2, p3, a, b, c, m):
    if n == 0:      # brak dysków
        return
    hanoi(n - 1, p1, p3, p2, a, b, c, m)
    for i in range(m):         # przenoszenie dysku
        if p1[i] != "|" + (m+1) * " ":
            for j in range(m - 1, -1, -1):
                if p3[j] == "|" + (m+1) * " ":
                    p1[i], p3[j] = p3[j], p1[i]
                    break
            break
    global licznik
    licznik += 1
    graf(m, a, b, c, licznik)
    hanoi(n - 1, p2, p1, p3, a, b, c, m)


menu()
