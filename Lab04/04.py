# Napisz program, który będzie pobierał od użytkownika liczbę n, a następnie wyświetli
# 2 ”trójkąty prostokątne” skierowane w lewą stronę od góry i do dołu z gwiazdek o wysokości
# podanej przez użytkownika.

n = int(input("Podaj wysokość: "))

for i in range(1, n+1):
    print((n-i)*" " + i * "*")

print("")

for i in range(n, 0, -1):
    print((n-i)*" " + i * "*")