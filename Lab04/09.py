# Napisz program, który pobierze od użytkownika hasło oraz liczbę n.
# Następnie poprosi użytkownika o podanie prawidłowego hasła n razy.
# W przypadku podania nieprawidłowego hasła, użytkownik powinien wpisywać je tak długo, aż poda prawidłowe.

haslo = str(input("Podaj hasło: "))
liczba = int(input("Podaj ilość powtórzeń: "))
tmp = str()

for i in range(1, liczba+1):
    tmp = str(input(str("Powtórz hasło "+str(i)+": ")))
    while tmp != haslo:
        tmp = str(input(str("Powtórz hasło " + str(i) + ": ")))
print("Dziękuję, podałeś prawidłowe hasło", liczba, "razy.")
