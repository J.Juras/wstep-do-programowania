# Napisz program, który pobierze od użytkownika liczbę n, a następnie wypisze n-tą liczbę Fibonacciego.

n = int(input("Podaj liczbę: "))

a, b = 0, 1
for i in range(1, n):
    a, b = b, a + b

if n == 0:
    print(n, "liczba Fibonacciego to:", a)
else:
    print(n, "liczba Fibonacciego to:", b)
