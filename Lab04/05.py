# Napisz program, który będzie pobierał od użytkownika liczbę n, a następnie wyświetli
# 2 ”trójkąty prostokątne” skierowane w prawą stronę od góry i dołu z gwiazdek o podanej wysokości przez użytkownika.

n = int(input("Podaj wysokość: "))

for i in range(1, n+1):
    print(i * "*")

print("")

for i in range(n, 0, -1):
    print(i * "*")
