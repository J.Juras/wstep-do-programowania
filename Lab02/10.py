#Napisz skrypt, w którym po podaniu dowolnej temperatury otoczenia pokaże komunikat informujący o tym że jest:
#- bardzo ciepło
#- ciepło
#- neutralnie
#- zimno
#- bardzo zimno
#Dodatkowo ustal zakres, którego temperatura nie może przekroczyć (minimum i maksimum temperatury).
#Wykorzystaj zagnieżdżoną instrukcję warunkową. Ustal (wg własnego uznania) i przechowuj w zmiennych
#wartości progów dla których wyświetla się odpowiedni komunikat

#temperatura
temp = int(input("Podaj temperaturę otoczenia: "))

#zakres
min_temp = -50
max_temp = 50

if temp >= min_temp and temp <= max_temp:
    if temp <= -5:
        print("Jest bardzo zimno.")
    elif temp <= 5:
        print("Jest zimno.")
    elif temp <= 15:
        print("Jest neutralnie.")
    elif temp <= 25:
        print("Jest ciepło.")
    else:
        print("Jest bardzo ciepło.")
else:
    print("Temperatura spoza zakresu.")