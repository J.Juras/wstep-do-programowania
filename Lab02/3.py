#Napisz skrypt, który po podaniu trzech liczb naturalnych wyświetli wartość
# największej z nich. Wykorzystaj zagnieżdżoną instrukcję warunkową.

a = int(input("First number: "))
b = int(input("Second number: "))
c = int(input("Third number: "))

if a > b:
    if a >= c:
        print("Number", a, "is the largest.")
if b > c:
    if b >= a:
        print("Number", b, "is the largest.")
if c > a:
    if c >= b:
        print("Number", c, "is the largest.")
if a == b == c:
    print("Numbers are the same.")