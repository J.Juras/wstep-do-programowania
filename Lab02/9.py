#Napisz skrypt, który wyświetli informację, czy dana liczba n jest jednocyfrowa,
#dwucyfrowa czy trzycyfrowa. W przeciwnym razie powinien pojawić się odpowiedni
#komunikat informujący, że podana liczba jest z poza zakresu. Uwzględnij również liczby ujemne.

n = int(input("Podaj liczbę całkowitą: "))

if n >= 0:
    if len(str(n)) <= 3:
        print("Liczba jest", str(len(str(n)))+"-cyfrowa.")
    else:
        print("Liczba spoza zakresu.")
else:
    if len(str(n)) <= 4:
        print("Liczba jest", str(len(str(n))-1)+"-cyfrowa.")
    else:
        print("Liczba spoza zakresu.")