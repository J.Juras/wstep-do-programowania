import math
#Napisz program, który wyznaczy miejsca zerowe funkcji kwadratowej.

#a =/= 0
a = float(input("Podaj a: "))
b = float(input("Podaj b: "))
c = float(input("Podaj c: "))

#Wzór na deltę
delta = b**2 - 4*a*c
print("Delta: ", delta)

if delta > 0:
    x1 = (-b - math.sqrt(delta))/2*a
    x2 = (-b + math.sqrt(delta))/2*a
    print("Miejsca zerowe:", str(x1)+", "+str(x2))
elif delta == 0:
    x = (-b)/2*a
    print("Miejsce zerowe:", x)
else:
    print("Brak miejsc zerowych.")