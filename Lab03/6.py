# Napisz skrypt, który dla dowolnej liczby w systemie dwójkowym wyświetli
# jego odpowiednik w systemie dziesiętnym. Wykonaj to zadanie bez korzystania
# z wbudowanych funkcji przeliczających na inny system liczbowy.

# zapis binarny
bin = int(input("Podaj liczbę w zapisie dwójkowym: "))
# zapis dziesiętny
dec = 0
# licznik
i = 1

while bin != 0:
    rem = bin % 10  # pozostaje reszta 1 lub 0
    dec += rem * i  # mnożenie tej reszty przez licznik, czyli 2 w kolejnych potęgach
    i = i*2  # kolejne potęgi 2
    bin = bin // 10  # dzielenie przez 10 bez reszty, "usuwa" ostatnią cyfrę

print("Zapis dziesiętny:", dec)
