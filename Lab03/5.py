# Napisz skrypt, który dla dwóch liczb zwróci informację jaki jest ich największy
# wspólny dzielnik, np. dla liczb 24 i 18, będzie to 6.
# Wykorzystaj do tego algorytm Euklidesa z odejmowaniem.

a = int(input("a: "))
b = int(input("b: "))

while True:
    if a > b:
        a = a - b
    elif a < b:
        b = b - a
    else:
        break

print("Największy możliwy dzielnik to:", a)
