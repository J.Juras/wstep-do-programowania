# Napisz skrypt, który będzie pobierał od użytkownika liczby
# dopóki nie zostanie podana wartość 0 (0 oznacza koniec pobierania).
# Następnie wyświetli sumę podanych wartości.

licznik = 0
n = int(input())
licznik += n
while n != 0:
    n = int(input())
    licznik += n
print(licznik)
