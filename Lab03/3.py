# Napisz skrypt, który będzie pobierał od użytkownika liczby dopóki
# nie zostanie podana wartość 0 (0 oznacza koniec pobierania).
# Następnie wyświetli ile z pośród podanych liczb jest dodatnich, a ile ujemnych.

dodatnie = 0
ujemne = 0

while True:
    n = int(input())
    if n < 0:
        ujemne += 1
    elif n > 0:
        dodatnie += 1
    else:
        break
print("dodatnie:", dodatnie, "ujemne:", ujemne)
