# Napisz skrypt, który dla liczby n wyznaczy największą liczbę m
# taką, że: 1 + 2 + 3 + ... + m <= n.

licznik = 0
m = 0
n = int(input())

while licznik <= n:
    m += 1
    licznik += m

print(m-1)